import * as React from "react";
import FooterMenu from '../containers/common/FooterMenu';
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'

export default class App extends React.Component<{ store: any }, {}> {

    render() {
        return (
            <div>
                <AddTodo />
                <VisibleTodoList />
                <FooterMenu />
            </div>
        );
    }
}