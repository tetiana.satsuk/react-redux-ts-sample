import * as React from 'react'
import { ListItem, ListItemText } from "material-ui/List";
import Checkbox from 'material-ui/Checkbox';

export interface TodoProps {
    id?: number;
    onClick: (event: React.MouseEvent<HTMLLIElement>) => void;
    completed: boolean;
    text: string;
}

const Todo = ({ onClick, completed, text }: TodoProps) => (
    <ListItem
        onClick={onClick}
    >
        <Checkbox
            checked={completed}
            tabIndex={-1}
            disableRipple
        />
        <ListItemText
            primary={text}
        />
    </ListItem>
);

export default Todo;
