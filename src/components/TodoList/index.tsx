import * as React from 'react'
import Todo, {TodoProps} from './todo'
import List from "material-ui/List";

export interface TodoListProps {
    todos: Array<TodoProps>;
    toggleTodo: Function;
}

const TodoList = ({ todos, toggleTodo }: TodoListProps) => (
    <List>
        {todos.map(todo =>
            <Todo
                key={todo.id}
                {...todo}
                onClick={() => toggleTodo(todo.id)}
            />
        )}
    </List>
)

export default TodoList;
