import * as React from "react";
import * as style from './css/footerMenu.css';
import Button from "material-ui/Button";
let Logo ="./public/images/cat.png";

const FooterMenu = (props: { goToFoo?: (event: React.MouseEvent<HTMLElement>) => void }) => (
    <div>
        <Button variant="raised" onClick={props.goToFoo}>Go To Foo</Button>
        <h1 className={style.title}>A Simple React Component Example with Typescript</h1>
        <div>
            <img src={Logo} />
        </div>
    </div>
);

export default FooterMenu;