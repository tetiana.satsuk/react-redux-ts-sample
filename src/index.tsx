import * as React from "react";
import * as ReactDOM from "react-dom";

import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import App from "./components/app";
import { Route } from 'react-router'

import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter, routerReducer, routerMiddleware } from 'react-router-redux'

const history = createHistory();
const middleware = routerMiddleware(history);

const store = createStore(
    combineReducers({
        ...rootReducer,
        router: routerReducer
    }),
    composeWithDevTools(
        applyMiddleware(middleware)
    )
);

const rootEl = document.getElementById("root");

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <Route exact path="/" component={App} />
                <Route path="/foo" component={App} />
            </div>
        </ConnectedRouter>
    </Provider>,
    rootEl
);
