import * as React from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../../actions/todos'
import Button from "material-ui/Button";

const AddTodo = ({ dispatch }) => {
    let input;

    return (
        <div>
            <form onSubmit={e => {
                e.preventDefault();
                if (!input.value.trim()) {
                    return
                }
                dispatch(addTodo(input.value));
                input.value = ''
            }}>
                <input ref={node => input = node} />
                <Button type="submit" variant="raised" color="primary">
                    Add Todo
                </Button>
            </form>
        </div>
    )
};

export default connect()(AddTodo)
