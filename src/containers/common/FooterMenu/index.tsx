import { connect } from 'react-redux'
import { push } from 'react-router-redux';
import FooterMenu from "../../../components/common/FooterMenu/index";

const mapDispatchToProps = (dispatch) => ({
    goToFoo: () => { dispatch(push('/foo')) }
});

export default connect(null, mapDispatchToProps)(FooterMenu);
