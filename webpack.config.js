var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var path = require("path");
var config = {
    mode: 'development',
    devtool: "source-map-loader",
    entry: ["./src/index.tsx"],
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: '/'
    },
    resolve: {
        extensions: [" ", ".ts", ".tsx", ".js"]
    },
    devServer: {
        host: '0.0.0.0',
        hot: true,
        port: 4000
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/,
                include: path.join(__dirname, 'src/components'),
                loaders: [
                    'style-loader',
                    {
                        loader: 'typings-for-css-modules-loader',
                        options: {
                            modules: true,
                            namedExport: true
                        }
                    }
                ]
            }

        ]
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            chunksSortMode: 'dependency',
            template: path.resolve(__dirname, 'index.html')
        })
    ]

};

module.exports = config;